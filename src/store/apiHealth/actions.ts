import {ActionTree} from 'vuex';
import {OrderAPI} from '@/backend/orderapi';
import {PaymentAPI} from '@/backend/paymentapi';
import {IApiHealth} from '@/store/apiHealth/index';

export const actions: ActionTree<IApiHealth, {}> = {
    async loadApiHealth({commit}) {
        if (await OrderAPI.heartBeat() === 'OrderAPI is up') {
            commit('setOrderApiHealth', true);
        } else {
            commit('setOrderApiHealth', false);
        }

        if (await PaymentAPI.heartBeat() === 'PaymentAPI is up') {
            commit('setPaymentApiHealth', true);
        } else {
            commit('setPaymentApiHealth', false);
        }
    },
};
