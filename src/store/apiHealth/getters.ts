import {GetterTree} from 'vuex';
import {IApiHealth} from '@/store/apiHealth/index';

export const getters: GetterTree<IApiHealth, {}> = {
    orderApiHealth(state): boolean {
        return state.orderAPI;
    },
    paymentApiHealth(state): boolean {
        return state.paymentAPI;
    },
};
