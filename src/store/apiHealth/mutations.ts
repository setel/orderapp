import {MutationTree} from 'vuex';
import {IApiHealth} from '@/store/apiHealth/index';

export const mutations: MutationTree<IApiHealth> = {
    setOrderApiHealth(state: IApiHealth, alive: boolean) {
        state.orderAPI = alive;
    },
    setPaymentApiHealth(state: IApiHealth, alive: boolean) {
        state.paymentAPI = alive;
    },
};
