import {Module} from 'vuex';
import {getters} from '@/store/apiHealth/getters';
import {actions} from '@/store/apiHealth/actions';
import {mutations} from '@/store/apiHealth/mutations';

export interface IApiHealth {
    orderAPI: boolean;
    paymentAPI: boolean;
}

export const state: IApiHealth = {
    orderAPI: false,
    paymentAPI: false,
};

const apiHealth: Module<IApiHealth, {}> = {
    state,
    getters,
    actions,
    mutations,
};

export default apiHealth;
