import {ActionTree} from 'vuex';
import {OrderAPI} from '@/backend/orderapi';

let isLoad = false;

export const actions: ActionTree<{ orders: Order[] }, {}> = {
    async loadOrders({state, commit}) {
        if (!isLoad) {
            const response = await OrderAPI.orders();
            if (typeof response !== 'string') {
                commit('setOrders', response);
                isLoad = true;
            }
        }
    },
    async createOrder({commit}, data: ICreateOrderRequest) {
        const order = await OrderAPI.createOrder(data);

        if (typeof order !== 'string') {
            commit('addOrder', order);
        }
    },
    async cancelOrder({commit}, oid: string) {
        const success = await OrderAPI.cancelOrder(oid);

        if (typeof success === 'boolean' && success) {
            commit('setOrdersStatus', {
                oid,
                states: 'cancelled',
            });
        }
    },
};
