import {Module} from 'vuex';
import {getters} from '@/store/orders/getters';
import {actions} from '@/store/orders/actions';
import {mutations} from '@/store/orders/mutations';

export const state: { orders: Order[] } = {
    orders: [],
};

const orders: Module<{ orders: Order[] }, {}> = {
    state,
    getters,
    actions,
    mutations,
};

export default orders;
