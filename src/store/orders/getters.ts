import {GetterTree} from 'vuex';

export const getters: GetterTree<{ orders: Order[] }, {}> = {
    orders(state): Order[] {
        return [...state.orders];
    },
};
