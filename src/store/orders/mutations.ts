import {MutationTree} from 'vuex';

export const mutations: MutationTree<{ orders: Order[] }> = {
    setOrders(state: { orders: Order[] }, data: Order[]) {
        const addList = data.filter((order) => !state.orders.some((stateOrder) => stateOrder._id === order._id));
        state.orders = [...addList, ...state.orders];
    },
    addOrder(state: { orders: Order[] }, data: Order) {
        state.orders = [...state.orders, data];
    },
    setOrdersStatus(state: { orders: Order[] }, data: {oid: string, states: string}) {
        state.orders = state.orders.map((order) => {
            if (order._id === data.oid) {
                order.states = data.states;
                return order;
            } else {
                return order;
            }
        });
    },
};
