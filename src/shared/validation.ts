import { Component, Vue } from 'vue-property-decorator';
import { validationMixin } from 'vuelidate';

@Component({
    mixins: [validationMixin],
})
export class Validation extends Vue {
    protected validationClass(data: {$dirty: boolean}, options?: {customMessage?: string, stopValidation?: boolean}) {
        if (options && options.stopValidation) {
            return '';
        } else if (data.$dirty) {
            return (this.validationState(data, options) && (!options || !options.customMessage)) ?
                'is-valid' : 'is-invalid';
        } else {
            return '';
        }
    }

    protected validationState(data: any, options?: {customMessage?: string, stopValidation?: boolean}) {
        if (options && options.stopValidation) {
            return true;
        } else if (data.$dirty) {
            let result: boolean = true;
            for (const key in data) {
                if (!key.startsWith('$') && !data[key]) {
                    result = false;
                    break;
                }
            }

            return result && (!options || !options.customMessage);
        } else {
            return true;
        }
    }
}
