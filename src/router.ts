import Vue from 'vue';
import Router from 'vue-router';
import {staticRoute} from './router/static.router';
import {authenticate} from '@/router/middleware.router';
import {homeRoute} from '@/router/home.router';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
      homeRoute,
      staticRoute,
  ],
});

router.beforeEach(authenticate);
export default router;
