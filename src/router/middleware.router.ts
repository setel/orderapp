export const authenticate = async (to: any, from: any, next: any) => {
    if (to.matched.some((record: {meta: {requiresAuth?: boolean, guest?: boolean}}) => record.meta.requiresAuth)) {
        if (!localStorage.getItem('token')) {
            next({name: 'Login'});
        } else {
            next();
        }
    } else if (to.matched.some((record: {meta: {requiresAuth?: boolean, guest?: boolean}}) => record.meta.guest)) {
        if (!localStorage.getItem('token')) {
            next();
        } else {
            next({name: 'Home'});
        }
    } else {
        next();
    }
};
