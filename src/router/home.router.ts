// Containers
import DefaultContainer from '@/components/DefaultContainer.vue';
import Orders from '@/views/Orders.vue';
import Order from '@/views/Order.vue';
import Create from '@/views/Create.vue';
import ApiHealth from '@/views/ApiHealth.vue';

export const homeRoute = {
    path: '/',
    redirect: '/orders',
    name: 'Home',
    component: DefaultContainer,
    children: [
        {
            path: '/orders',
            name: 'Orders',
            component: Orders,
        },
        {
            path: '/orders/:id',
            meta: { label: 'Order Details'},
            name: 'Order',
            component: Order,
        },
        {
            path: '/create',
            name: 'Create',
            component: Create,
        },
        {
            path: '/apiHealth',
            name: 'API',
            component: ApiHealth,
        },
    ],
    meta: {
        requiresAuth: true,
    },
};
