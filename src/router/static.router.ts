import Login from '@/views/Login.vue';

export const staticRoute = {
    path: '/',
    redirect: '/login',
    name: 'Static',
    component: {
        render(c: any) { return c('router-view'); },
    },
    children: [
        {
            path: 'login',
            name: 'Login',
            component: Login,
            meta: {
                guest: true,
            },
        },
    ],
};
