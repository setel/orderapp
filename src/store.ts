import Vue from 'vue';
import Vuex from 'vuex';
import orders from '@/store/orders';
import apiHealth from '@/store/apiHealth';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  modules: {
    orders,
    apiHealth,
  },
});
