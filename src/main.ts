import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import {registerMiddleware} from '@/backend/middleware';

Vue.config.productionTip = false;
Vue.use(BootstrapVue);
registerMiddleware();

store.dispatch('loadApiHealth').then(() => {
  window.setInterval(async () => {
    await store.dispatch('loadApiHealth');
  }, 30000);
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
