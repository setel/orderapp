import router from '../router';
import { Service } from 'axios-middleware';
import axios, {AxiosError, AxiosPromise, AxiosRequestConfig, AxiosResponse} from 'axios';
import {orderAPIConfig} from '@/backend/orderapi/config';

export const registerMiddleware = () => {
    const service = new Service(axios);

    service.register({
        onRequest(config: AxiosRequestConfig) {
            // console.log('onRequest');
            if (localStorage.getItem('token') && config.url && config.url.startsWith(orderAPIConfig.API_URL)) {
                config.headers.Authorization = `${localStorage.getItem('token')}`;
                config.headers.withCredentials = true;
            }
            return config;
        },
        onSync(promise: AxiosPromise) {
            // console.log('onSync');
            return promise;
        },
        onResponse(response: AxiosResponse) {
            // console.log('onResponse');
            return response;
        },
        onResponseError(error: AxiosError) {
            // console.log('onResponseError');
            if (error.response && error.response.status === 403) {
                router.replace({name: 'Login'});
            } else if (error.response) {
                throw Error(JSON.parse(error.response.data).error.message);
            }
        },
    });
};
