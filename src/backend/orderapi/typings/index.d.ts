interface ILoginRequest {
    email: string
}

interface ICreateOrderRequest {
    productId: string,
    productName: string,
    price: number
}
