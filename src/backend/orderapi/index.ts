import axios, {AxiosResponse} from 'axios';
import {orderAPIConfig} from './config';

export class OrderAPI {
    public static async heartBeat(): Promise<string> {
        try {
            return (await axios.get(`${orderAPIConfig.API_URL}`)).data;
        } catch (error) {
            return error.message;
        }
    }

    public static async login(request: ILoginRequest): Promise<boolean | string> {
        try {
            const response: AxiosResponse = await axios.post(`${orderAPIConfig.API_URL}/login`, request);
            if (response.status === 200) {
                if (response.data) {
                    localStorage.setItem('token', response.data);
                }

                return true;
            } else {
                return false;
            }
        } catch (error) {
            return error.message;
        }
    }

    public static async orders(): Promise<Order[] | string> {
        try {
            return (await axios.get(`${orderAPIConfig.API_URL}/orders`)).data;
        } catch (error) {
            return error.message;
        }
    }

    public static async createOrder(request: ICreateOrderRequest): Promise<Order | string> {
        try {
            return (await axios.post(`${orderAPIConfig.API_URL}/orders/create`, request)).data;
        } catch (error) {
            return error.message;
        }
    }

    public static async cancelOrder(oid: string): Promise<boolean | string> {
        try {
            await axios.post(`${orderAPIConfig.API_URL}/orders/cancel/${oid}`);
            return true;
        } catch (error) {
            return error.message;
        }
    }
}
