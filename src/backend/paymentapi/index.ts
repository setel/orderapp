import axios from 'axios';
import {paymentAPIConfig} from '@/backend/paymentapi/config';

export class PaymentAPI {
    public static async heartBeat(): Promise<string> {
        try {
            return (await axios.get(`${paymentAPIConfig.API_URL}`)).data;
        } catch (error) {
            return error.message;
        }
    }
}
